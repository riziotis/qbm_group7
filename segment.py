#!/usr/bin/env python3

import os
import argparse

import numpy as np
import h5py
import cv2
import skimage
import torch
import matplotlib.pyplot as plt

from skimage.segmentation import watershed
from skimage.measure import label
from skimage.metrics import adapted_rand_error
from pytorch3dunet.unet3d.model import UNet2D
from stardist.models import StarDist2D

# suppress tf messages
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

def normalize_array(np_array):
    """ normalization function """
    mean, std = np.mean(np_array), np.std(np_array)
    normalized = (np_array - mean) / std

    return normalized 

def segment_nuclei(nuclei_array):
    """ takes a np array and segments the nuclei from it """

    # create a guassian blur on image
    blur = cv2.GaussianBlur(nuclei_array, (5,5), 0)
    ret3, th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    th3 = np.array(th3, dtype='float32')

    # label with skkimage and return the nuclei
    return label(th3)

def segment_nuclei_stardist(nuclei_array):
    """ perform nuclei segmentation with stardist """

    normalized = normalize_array(nuclei_array)
    model = StarDist2D.from_pretrained('2D_versatile_fluo')
    labels, _ = model.predict_instances(normalized)

    return labels

def export_array(np_array, filename=None, cmap='prism'):
    plt.figure(figsize=(5,5))
    plt.imshow(np_array, cmap)
    if filename:
        plt.savefig(filename)

def export_arrays(folder, arrays):
    """ given folder and arrays, saves images """
    for name, array in arrays.items():
        filepath = os.path.join(folder, f'{name}.png')
        plt.figure(figsize=(5,5))
        cmap = 'gist_gray' if 'raw' in name else 'prism'
        plt.imshow(array, cmap)
        plt.axis('off')
        plt.savefig(filepath, bbox_inches='tight')
        plt.close()
    
    return

def export_metrics(folder, metrics, avg):
    """ given filepath and metrics, exports to file """

    f = open(os.path.join(folder, 'metrics.txt'), "w")
    for m, value in metrics.items():
        f.write(f"{m}: {value:.3}\n")
        avg[m].append(value) # add to avg tracking
    f.close()

def export_results(results, output_path):
    """ given a result dictionary, export result files and metrics """

    # track for average
    avgs = {
        'RandErr': [],
        'Precision': [],
        'Recall': []
    }
    
    # export individual
    for file_key, file_results in results.items():
        # make subdir of results for filename
        sub_folder = os.path.join(output_path, file_key)
        if not os.path.exists(sub_folder):
            os.mkdir(sub_folder)
        
        export_metrics(sub_folder, file_results['metrics'], avgs)
        export_arrays(sub_folder, file_results['arrays'])

    # export averge 
    f = open(os.path.join(output_path, 'summary_metrics.txt'), "w")
    for m, values in avgs.items():
        avg_value = sum(values) / len(values)
        f.write(f"{m}: {avg_value:.3}\n")
    f.close()

    return

def segment_cells(serum_array):
    """ takes the serum channel and segments cell boundries using unet3d """
    
    # Load model from provided config yaml
    model = UNet2D(
        in_channels=1,
        out_channels=2,
        layer_order="gcr", 
        f_maps=[32, 64, 128, 256, 512],
        num_groups=8,
        final_sigmoid=True,
        is_segmentation=True,
        testing=False
    )
    pretrained_model = os.path.join("resources", "best_model_state_dict.pth")
    model.load_state_dict(torch.load(pretrained_model))
    model.eval()
    model.testing = True
    model = model.to('cpu')

    #Normalize data
    transformed = np.array(serum_array)[np.newaxis, np.newaxis, np.newaxis, :, :]
    mean, std = np.mean(transformed), np.std(transformed)
    normalized = (transformed - mean) / std

    #Convert to torch tensor
    tensor_obj = torch.from_numpy(normalized).float()
    out = np.squeeze(model(tensor_obj).detach().cpu().numpy())
    foreground = out[0] 
    cell_boundary = out[1]

    return foreground, cell_boundary

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--stardist', dest='USE_STARDIST', action='store_true')
    parser.set_defaults(USE_STARDIST=False)
    args = parser.parse_args()

    input_dir = "CovidGroundTruth"
    channels = {
        'serum': 0,
        'infection': 1,  # ignore
        'nuclei': 2
    }
    USE_STARDIST = args.USE_STARDIST

    if USE_STARDIST:
        out_dir = "stardist_results"
    else:
        out_dir = "results"

    out_path = os.path.join(os.getcwd(), out_dir)
    if not os.path.exists(out_path):
        os.mkdir(out_path)

    results = {}

    i = 0
    output_text = ""
    for input_file in os.listdir(input_dir):
        f = h5py.File(os.path.join(input_dir, input_file))
        if 'raw' not in f:
            continue # prevent error on extra files

        # grab truth and raw data
        truth = f['cells']
        raw = f['raw']

        # perform segmentation on nuclei
        nuclei_img_array = np.array(raw[channels['nuclei']][:,:])
        if USE_STARDIST:
            nuclei_segmented = segment_nuclei_stardist(nuclei_img_array)
        else:     
            nuclei_segmented = segment_nuclei(nuclei_img_array)

        # perform segmentation on cell boundries
        serum_img_array = np.array(raw[channels['serum']][:,:])
        foreground, cell_boundary = segment_cells(serum_img_array)

        # convert foreground to bool mask
        foreground_bool = (foreground > 0.6).astype(int)         

        # perform watershed algorithm
        cells = watershed(cell_boundary, markers=nuclei_segmented, mask=foreground_bool)

        # Rand comparison against ground truth
        rand_err, precision, recall = adapted_rand_error(image_true=np.array(truth, dtype='int'), image_test=np.array(cells, dtype='int'))
        
        # remove .hd5 extension to get unique file key
        file_key = os.path.splitext(input_file)[0]
        results[file_key] = {
            'metrics': {
                'RandErr': rand_err,
                'Precision': precision,
                'Recall': recall,
            },
            'arrays': {
                'raw_nuclei': nuclei_img_array ,
                'seg_nuclei': nuclei_segmented,
                'raw_serum': serum_img_array,
                'seg_boundary': cell_boundary,
                'seg_cells': cells,
                'truth': truth
            }
        }

        i+=1 # increment after success

    export_results(results, out_path)

if __name__ == "__main__":
    main()
