# predoc-course-2020

## Image Analysis Pipeline

You will design an image analysis pipeline for immunofluorescence images of COVID-infected cells published in [Microscopy-based assay for semi-quantitative detection of SARS-CoV-2 specific antibodies in human sera](https://www.biorxiv.org/content/10.1101/2020.06.15.152587v2). 

## Instructions to Run Pipeline

Requirements:
* Python3.6

Clone the repo and run:
```bash
python3 -m pip install -r requirements.txt
```
to install required packages. (Note: if you get an error on the stardist install you may need to install llvm>=9.0 via brew or your os package manager)

Then, navigate to the `pytorch-3dunet` folder and run:
```bash
sudo python3 setup.py install
```
to install the 3dunet tool.

Finally, run:
```bash
python3 segment.py
```

Results should appear in the `results` folder and print to the console.

### Troubleshooting StarDist Install
(only tested on Ubuntu)
1. Install llvm-9 via apt-get (Ubuntu) or brew (Mac)
1. Set the LLVM_CONFIG env variable and verify:
    ```bash
    export LLVM_CONFIG=$(which llvm-config-9)
    echo LLVM_CONFIG # should show a path
    ```
1. Remove h5py if you have it and force reinstall/downgrade to h5py==2.10.0 (https://github.com/tensorflow/tensorflow/issues/44467)
